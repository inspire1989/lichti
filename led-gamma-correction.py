#!/usr/bin/env python3

import pathlib

gamma_value = 2.8  #Correction factor
max_in  = 1023  # Top end of INPUT range
max_out = 1023  # Top end of OUTPUT range

output = "// USE led-gamma-correction.py TO REGENERATE THIS FILE!\n\n\
#ifndef LEDS_GAMMA_HPP\n\
#define LEDS_GAMMA_HPP\n\n\
#include <pgmspace.h>\n\n\
const float gamma_value = " + str(gamma_value) + "F;\n\
const int max_in = " + str(max_in) + ";\n\
const int max_out = " + str(max_out) + ";\n\n\
const int PROGMEM gamma_array[] = { "

for i in range(max_in+1):
    if i>0:
        output += ','

    if i % 16 == 0:
        output += '\n'

    output += str(round(pow(i / max_in, gamma_value) * max_out))
    
output += " };\n\n\
#endif"

print(output)
print()

f = open(str(pathlib.Path(__file__).parent.absolute()) + "/main/LEDs_gamma.hpp", "w")
f.write(output)
f.close()

print("File LEDs_gamma.hpp written!")
