#ifndef WIFI_HPP
#define WIFI_HPP

extern const char* wifi_ssid;
extern const char* wifi_password;

extern const char* raspberry_hostname;

void wifi_check_reconnect();

void wifi_setup();

#endif // WIFI_HPP
