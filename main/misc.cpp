#include "misc.hpp"

#include <WiFiUdp.h>

// UTC+1h
const long utcOffsetInSeconds = 3600;

char daysOfTheWeek[7][12] = {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

WiFiUDP ntpUDP;
NTPClient ntp_timeClient(ntpUDP, "pool.ntp.org", utcOffsetInSeconds);

void ntp_time_print() {
  Serial.print("NTP time: ");
  Serial.println(ntp_timeClient.getFormattedDate());
}

void setup_random() {
  randomSeed(analogRead(0));
}
