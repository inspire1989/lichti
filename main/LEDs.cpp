#include "LEDs.hpp"
#include "LEDs_gamma.hpp"
#include "mqtt.hpp"
#include <pgmspace.h>

#include <ESP8266WiFi.h>

const int PSU_ENABLE   = 15; // GPIO15, PSU enable pin

const int LED_RED   = 4; // GPIO4, red
const int LED_GREEN = 12; // GPIO12, green
const int LED_BLUE  = 5; // GPIO5, blue
const int LED_WHITE = 14; // GPIO14, white

bool leds_state = false;

int leds_brightness = 255;
int leds_red = 255;
int leds_green = 255;
int leds_blue = 255;
int leds_white = 0;
String leds_effect("Rainbow");
int rainbow_direction = 0;

float pwm_value_red = 0;
float pwm_value_green = 0;
float pwm_value_blue = 0;
float pwm_value_white = 0;

void leds_update(bool leds_state_new, int leds_brightness_new, int leds_red_new, int leds_green_new, int leds_blue_new, int leds_white_new, String leds_effect_new) {

  if (!leds_state && leds_state_new) {
    // turn on
    
    // PSU on
    digitalWrite(PSU_ENABLE, 1);
    
    delay(10); // wait until power is on
  } else if (leds_state && !leds_state_new) {
    // turn off
    
    // PSU off
    digitalWrite(PSU_ENABLE, 0);
    
    delay(10); // wait until power is off
    
    // all LEDs off
    analogWrite(LED_RED, 0);
    analogWrite(LED_GREEN, 0);
    analogWrite(LED_BLUE, 0);
    analogWrite(LED_WHITE, 0);
  }

  // set new random direction for rainbow
  if ((leds_state != leds_state_new) || (leds_effect != leds_effect_new)) {
    rainbow_direction = random(0,2);
  }
  
  // store new values
  leds_state = leds_state_new;
  leds_brightness = leds_brightness_new;
  leds_red = leds_red_new;
  leds_green = leds_green_new;
  leds_blue = leds_blue_new;
  leds_white = leds_white_new;
  leds_effect = leds_effect_new;
 
  // if effects string is empty, reset to "Manual"
  if (leds_effect.equals("")) {
    leds_effect = "Manual";
  }

  // if LEDs are on control their color
  if (leds_state && leds_effect.equals("Manual")) {
    leds_setColor();
  }
}

void leds_setColor() {
  // STEP 1 - SCALE UP: inputs: Homie color and brightness, outputs: PWM values for all channels
    // Homie sends values between 0 and 255 for color and brightness. Brightness value is the same for all RGB channels. 
    // Calculate color*brightness and scale up from 0..255 to 0..1023 values.
    // 1023 is maximum for PWM, equals 255(color)*255(brightness).
    const float MAX_HOMIE_BRIGHTNESS = 255;
    const float MAX_HOMIE_COLOR_VALUE = 255;
    const float MAX_PWM_VALUE = 1023;
    float pwm_value_red_float = leds_red * leds_brightness * MAX_PWM_VALUE/(MAX_HOMIE_COLOR_VALUE*MAX_HOMIE_BRIGHTNESS);;
    float pwm_value_green_float = leds_green * leds_brightness * MAX_PWM_VALUE/(MAX_HOMIE_COLOR_VALUE*MAX_HOMIE_BRIGHTNESS);
    float pwm_value_blue_float = leds_blue * leds_brightness * MAX_PWM_VALUE/(MAX_HOMIE_COLOR_VALUE*MAX_HOMIE_BRIGHTNESS);
    float pwm_value_white_float = leds_white * MAX_PWM_VALUE/MAX_HOMIE_BRIGHTNESS;
    if (pwm_value_red_float > 1023) pwm_value_red_float = 1023;
    if (pwm_value_green_float > 1023) pwm_value_green_float = 1023;
    if (pwm_value_blue_float > 1023) pwm_value_blue_float = 1023;
    if (pwm_value_white_float > 1023) pwm_value_white_float = 1023;

    // STEP 2 - GAMMA CORRECTION: outputs: PWM values for all channels with gamma correction
    pwm_value_red_float = pgm_read_word(gamma_array+(int)round(pwm_value_red_float));
    pwm_value_green_float = pgm_read_word(gamma_array+(int)round(pwm_value_green_float));
    pwm_value_blue_float = pgm_read_word(gamma_array+(int)round(pwm_value_blue_float));
    pwm_value_white_float = pgm_read_word(gamma_array+(int)round(pwm_value_white_float));

    // STEP 3 - POWER LIMIT: outputs: PWM values for all channels with gamma correction and power limit
    // Limit the PWM power to half of maximum. Maximum value would be 1023*4=4092.
    // If value is above 1500 then start limiting until maximum value 2000 is reached. Color ratios have to be kept the same.
    // --> Linear function through points (1500,1500) and (4092,2000). See https://www.mathepower.com/lineare_funktionen.php.
    const float PWM_POWER_SATURATION_START_INPUT = 1500;
    const float PWM_POWER_SATURATION_START_OUTPUT = 1500;
    
    const float PWM_POWER_SATURATION_MAX_INPUT = 4092;
    const float PWM_POWER_SATURATION_MAX_OUTPUT = 2000;

    // calculate summarized PWM powerPWM_POWER_SATURATION_START_INPUT
    const float pwm_total_input = pwm_value_red_float + pwm_value_green_float + pwm_value_blue_float + pwm_value_white_float;
    if (pwm_total_input > PWM_POWER_SATURATION_START_INPUT) {
      const float m = (PWM_POWER_SATURATION_MAX_OUTPUT-PWM_POWER_SATURATION_START_OUTPUT)/(PWM_POWER_SATURATION_MAX_INPUT-PWM_POWER_SATURATION_START_INPUT);
      const float b = PWM_POWER_SATURATION_START_OUTPUT-(m*PWM_POWER_SATURATION_START_INPUT);
      const float power_limit_factor = m*pwm_total_input+b;

      // scale all channels with respect to power limit factor while keeping their ratios
      pwm_value_red_float = pwm_value_red_float * power_limit_factor / pwm_total_input;
      pwm_value_green_float = pwm_value_green_float * power_limit_factor / pwm_total_input;
      pwm_value_blue_float = pwm_value_blue_float * power_limit_factor / pwm_total_input;
      pwm_value_white_float = pwm_value_white_float * power_limit_factor / pwm_total_input;
    }
    
    // check if new power values within range
    const float pwm_total_output = pwm_value_red_float + pwm_value_green_float + pwm_value_blue_float + pwm_value_white_float;
    bool power_check = false;
    if (pwm_total_output < PWM_POWER_SATURATION_MAX_OUTPUT+100) { // everything is okay - add small buffer (100)
      power_check = true;
    } else {
      power_check = false;
    }

    // STEP 4: if power is okay, use new PWM values
    if (power_check) {
      pwm_value_red = (int)round(pwm_value_red_float);
      pwm_value_green = (int)round(pwm_value_green_float);
      pwm_value_blue = (int)round(pwm_value_blue_float);
      pwm_value_white = (int)round(pwm_value_white_float);
      
      analogWrite(LED_RED, pwm_value_red);
      analogWrite(LED_GREEN, pwm_value_green);
      analogWrite(LED_BLUE, pwm_value_blue);
      analogWrite(LED_WHITE, pwm_value_white);
    } else { // new total power is too high - this shouldn't happen - turn of everything
      pwm_value_red = 0;
      pwm_value_green = 0;
      pwm_value_blue = 0;
      pwm_value_white = 0;
      
      analogWrite(LED_RED, pwm_value_red);
      analogWrite(LED_GREEN, pwm_value_green);
      analogWrite(LED_BLUE, pwm_value_blue);
      analogWrite(LED_WHITE, pwm_value_white);
    }
}

void get_random_color(unsigned int& red, unsigned int& green, unsigned int& blue) {
  static unsigned int color_disable = 0; // 0: red dimmed, 1: green dimmed, 2: blue dimmed
  
  red = random(100,256);
  green = random(100,256);
  blue = random(100,256);

  // make sure that we don't get white
  switch (color_disable) {
    case 0:
      red = red/3;
      color_disable = random(1,3);
      break;
    case 1:
      green = green/3;
      color_disable = random(0,2);
      if (color_disable == 1) {
        color_disable = 2;
      }
      break;
    case 2:
      blue = blue/3;
      color_disable = random(0,2);
      break;
    default: color_disable = 0; break;
  }

  // make sure brightness is good
  if (red + green + blue < 500) {
    if (red > green && red > blue) {
      red = random(200,256);
    } else if (green > red && green > blue) {
      green = random(200,255);
    } else if (blue > red && blue > green) {
      blue = random(200,256);
    }
  }
}

void leds_refresh_disco() {
  static unsigned int divider = 0;
  const unsigned int interval = 40; // function called every 10 ms --> change every 400 ms
  
  if (divider == interval) { 
    unsigned int red = 0;
    unsigned int green = 0;
    unsigned int blue = 0;

    get_random_color(red, green, blue);
    
    leds_update(leds_getState(), 255, red, green, blue, 0, leds_getEffect());
    divider = 0;
  } else {
    ++divider;
  }
}

void leds_refresh_random() {
  static unsigned int divider = 0;
  const unsigned int interval = 1500; // function called every 10 ms --> change every 15 s
  
  static unsigned int last_red = 0;
  static unsigned int last_green = 0;
  static unsigned int last_blue = 0;
  
  static unsigned int current_red = 0;
  static unsigned int current_green = 0;
  static unsigned int current_blue = 0;
  
  static unsigned int target_red = 0;
  static unsigned int target_green = 0;
  static unsigned int target_blue = 0;
  
  if (divider == 0) {
    last_red = target_red;
    last_green = target_green;
    last_blue = target_blue;
    
    // get new target color
    get_random_color(target_red, target_green, target_blue);

    // make sure we don't move through white
    unsigned int random_color = random(0,3);
    switch(random_color) {
      case 0:
        target_red = 0;
        break;
      case 1:
        target_green = 0;
        break;
      case 2:
        target_blue = 0;
        break;
      default:
        break;
    }
    
    ++divider;
  } else if (divider == interval) { 
    divider = 0;
  } else {
    ++divider;
  }
  
  if (divider != 0)  {
    current_red = (unsigned int) round(divider*((float)target_red - last_red) / (float)(interval) + last_red);
    current_green = (unsigned int) round(divider*((float)target_green - last_green) / (float)(interval) + last_green);
    current_blue = (unsigned int) round(divider*((float)target_blue - last_blue) / (float)(interval) + last_blue);
  
    leds_update(leds_getState(), 255, current_red, current_green, current_blue, 0, leds_getEffect());
  }
}

void leds_refresh_rainbow() {
  static int divider = 0;
  const unsigned int interval = 10; // function called every 10 ms --> change every 100ms
  static unsigned int new_red = 0;
  static unsigned int new_green = 0;
  static unsigned int new_blue = 0;

  static int section = 0;
  static int i = 0;

  if (divider == 0) { // slow down loop

    if (rainbow_direction == 0) {
      if (section == 0) {
        new_red = i; // fade up
        new_green =  255 - i; // fade down
        new_blue = 0; // do nothing
      } else if (section == 1) {
        new_red = 255 - i; // fade down
        new_green = 0; // do nothing
        new_blue = i; // fade up
      } else {
        new_red = 0; // do nothing
        new_green = i; // fade up
        new_blue = 255 - i; // fade down
      }
    } else {
       if (section == 0) {
        new_red = 0; // do nothing
        new_green = 255-i; // fade down
        new_blue = i; // fade up
      } else if (section == 1) {
        new_red = i; // fade up
        new_green = 0; // do nothing
        new_blue = 255 - i; // fade down
      } else {
        new_red = 255 - i; // fade down
        new_green = i; // fade up
        new_blue = 0; // do nothing
      }
    }
    
    leds_update(leds_getState(), 255, new_red, new_green, new_blue, 0, leds_getEffect());

    i = (i+1)%256;
    if (i == 0) {
      section = (section+1)%3;
    }
  }

  if (divider == interval) { 
    divider = 0;
  } else {
    ++divider;
  }
}

void leds_refresh_effect() {
  // if LEDs are on control their color
  if (leds_state && !leds_effect.equals("Manual")) {
    if (leds_effect.equals("Disco")) {
      leds_refresh_disco();
      leds_setColor();
    } else if (leds_effect.equals("Random")) {
      leds_refresh_random();
      leds_setColor();
    } else if (leds_effect.equals("Rainbow")) {
      leds_refresh_rainbow();
      leds_setColor();
    }

    // publish new mqtt state every second
    static unsigned int i = 0;
    if (i == 100) {
      mqtt_sendState();
      i = 0;
    } else {
      ++i;
    }
  }
}

void leds_setup() {
  pinMode(LED_BUILTIN, OUTPUT);
  
  pinMode(PSU_ENABLE, OUTPUT);
  
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GREEN, OUTPUT);
  pinMode(LED_BLUE, OUTPUT);
  pinMode(LED_WHITE, OUTPUT);

  analogWriteFreq(1000); // 1KHz is default

  // PSU off
  digitalWrite(PSU_ENABLE, 0);
  
  // all off by default
  analogWrite(LED_RED, 0);
  analogWrite(LED_GREEN, 0);
  analogWrite(LED_BLUE, 0);
  analogWrite(LED_WHITE, 0);
}

bool leds_getState() {
  return leds_state;
}

int leds_getBrightness() {
  return leds_brightness;
}

int leds_getRed() {
  return leds_red;
}

int leds_getGreen() {
  return leds_green;
}

int leds_getBlue() {
  return leds_blue;
}

int leds_getWhite() {
  return leds_white;
}

String leds_getEffect() {
  return leds_effect;
}

int leds_getPWMValueRed() {
  return pwm_value_red;
}

int leds_getPWMValueGreen() {
  return pwm_value_green;
}

int leds_getPWMValueBlue() {
  return pwm_value_blue;
}

int leds_getPWMValueWhite() {
  return pwm_value_white;
}
