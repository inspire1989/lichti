#ifndef LEDS_HPP
#define LEDS_HPP

#include <WString.h>

void leds_update(bool leds_state, int leds_brightness, int leds_red_new, int leds_green_new, int leds_blue_new, int leds_white_new, String leds_effect_new);
void leds_setColor();

void leds_refresh_effect();
void leds_setup();

bool leds_getState();
int leds_getBrightness();
int leds_getRed();
int leds_getGreen();
int leds_getBlue();
int leds_getWhite();
String leds_getEffect();


int leds_getPWMValueRed();
int leds_getPWMValueGreen();
int leds_getPWMValueBlue();
int leds_getPWMValueWhite();

#endif // LEDS_HPP
