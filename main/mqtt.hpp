#ifndef MQTT_HPP
#define MQTT_HPP

#include <ESP8266WiFi.h>
#include <PubSubClient.h>

#define MQTT_CLIENTNAME "Lichti"

#define MQTT_BROKER "192.168.178.45" // RPI

#define MQTT_TOPIC_STATE "home/lichti/state"
#define MQTT_TOPIC_STATE_SET "home/lichti/state/set"

#define MQTT_PAYLOAD_ON "ON"
#define MQTT_PAYLOAD_OFF "OFF"

extern WiFiClient espClient;
extern PubSubClient mqtt_client;

void mqtt_sendState();

void mqtt_check_reconnect();

void mqtt_setup();

#endif // MQTT_HPP
