#include "webserver.hpp"
#include "LEDs.hpp"

ESP8266WebServer webserver(80);

void webserver_handleRoot() {
  const String message = "Lichti is happy to see you :)";
  const String leds_message = 
    String("\n\nLEDS:") + 
    String("\n- state: ") + String(leds_getState()) + 
    String("\n- brightness: ") + String(leds_getBrightness()) + 
    String("\n- rgb: [") + String(leds_getRed()) + String(", ") + String(leds_getGreen()) + String(", ") + String(leds_getBlue()) + String("]") + 
    String("\n- white: ") + String(leds_getWhite()) + 
    String("\n- PWM: [") + String(leds_getPWMValueRed()) + String(", ") + String(leds_getPWMValueGreen()) + String(", ") + String(leds_getPWMValueBlue()) + String(", ") + String(leds_getPWMValueWhite()) + String("]") + 
    String("\n- effect: ") + String(leds_getEffect());
  
  webserver.send(200, "text/plain", message + leds_message);
}

void webserver_setup() {
  webserver.on("/", webserver_handleRoot) ;
  webserver.begin();
  Serial.println("Webserver started");
}
