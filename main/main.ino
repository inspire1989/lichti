#include "wifi.hpp"
#include "webserver.hpp"
#include "mqtt.hpp"
#include "LEDs.hpp"
#include "OTA.hpp"
#include "misc.hpp"

// execution frequency [ms]
const long execFreqBuiltInLED = 1000;
const long execFreqLEDs = 10;
const long execFreqWifiCheck = 1000;
const long execFreqNTP = 100000;
const long execFreqMQTT = 5000;
const long execFreqMQTTCheck = 100;
const long execFreqOTA = 100;

// milli seconds
long lastLoopExecutionBuiltInLED = 0;
long lastLoopExecutionLEDs = 0;
long lastLoopExecutionWifiCheck = 0;
long lastLoopExecutionNTP = 0;
long lastLoopExecutionMQTT = 0;
long lastLoopExecutionMQTTCheck = 0;
long lastLoopExecutionOTA = 0;

void setup() {
  Serial.begin(115200);
  Serial.println("");
  Serial.println("Hello :)");

  setup_random();

  leds_setup();

  wifi_setup();

  ota_setup();

  ntp_timeClient.begin();
  ntp_timeClient.update();
  ntp_time_print();

  webserver_setup();

  mqtt_setup();
  mqtt_sendState();
  mqtt_client.loop();
}

void loop() {
  long currentExecution = millis();

  // toggle built-in LED
  if (currentExecution - lastLoopExecutionBuiltInLED > execFreqBuiltInLED) {
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));

    lastLoopExecutionBuiltInLED = currentExecution;
  }

  currentExecution = millis();

  // check WIFI
  if (currentExecution - lastLoopExecutionWifiCheck > execFreqWifiCheck) {
    wifi_check_reconnect();

    lastLoopExecutionWifiCheck = currentExecution;
  }

  currentExecution = millis();

  // check NTP
  if (currentExecution - lastLoopExecutionNTP > execFreqNTP) {
    ntp_timeClient.update();

    ntp_time_print();

    lastLoopExecutionNTP = currentExecution;
  }

  currentExecution = millis();

  // MQTT: check if reconnect necessary
  if (currentExecution - lastLoopExecutionMQTTCheck > execFreqMQTTCheck) {
    mqtt_check_reconnect();

    lastLoopExecutionMQTTCheck = currentExecution;
  }

  currentExecution = millis();

  // MQTT: publish state
  if (currentExecution - lastLoopExecutionMQTT > execFreqMQTT) {
    mqtt_sendState();

    lastLoopExecutionMQTT = currentExecution;
  }

  currentExecution = millis();

  // LED effect update
  if (currentExecution - lastLoopExecutionLEDs > execFreqLEDs) {
    leds_refresh_effect();

    lastLoopExecutionLEDs = currentExecution;
  }

  // MQTT: handle data I/O
  mqtt_client.loop();
  
  // handle webserver communication
  webserver.handleClient();
  
  // handle OTA
  ota_handle();
}
