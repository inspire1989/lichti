#ifndef MISC_HPP
#define MISC_HPP

#include "NTPClient-master/NTPClient.h"

extern NTPClient ntp_timeClient;

void ntp_time_print();

void setup_random();

#endif // MISC_HPP
