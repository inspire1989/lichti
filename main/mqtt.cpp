#include "mqtt.hpp"
#include "LEDs.hpp"
#include "misc.hpp"

#include <ArduinoJson.h>

WiFiClient espClient;
PubSubClient mqtt_client(espClient);
long lastMsg = 0;
int value = 0;

const int BUFFER_SIZE = JSON_OBJECT_SIZE(50);

void mqtt_subscribe() {
  mqtt_client.subscribe(MQTT_TOPIC_STATE_SET);
}

void mqtt_sendState() {
  StaticJsonDocument<BUFFER_SIZE> doc;

  doc["state"] = (leds_getState()) ? MQTT_PAYLOAD_ON : MQTT_PAYLOAD_OFF;

  doc["brightness"] = leds_getBrightness();
  doc["color"]["r"] = leds_getRed();
  doc["color"]["g"] = leds_getGreen();
  doc["color"]["b"] = leds_getBlue();
  doc["white_value"] = leds_getWhite();
  doc["effect"] = leds_getEffect();

  char buffer[512];
  const size_t n = serializeJson(doc, buffer);

  mqtt_client.publish(MQTT_TOPIC_STATE, (const uint8_t*)buffer, n, true);
}

bool processJson(char* message) {
  StaticJsonDocument<BUFFER_SIZE> doc;

  // read values
  bool leds_state = leds_getState();
  int leds_brightness = leds_getBrightness();
  int leds_red = leds_getRed();
  int leds_green = leds_getGreen();
  int leds_blue = leds_getBlue();
  int leds_white = leds_getWhite();
  String leds_effect = leds_getEffect();

  // indicator for manual interaction. Will be set to true when user makes manual change to color or brightness or white.
  bool leds_manual_change = false;
  
  DeserializationError error = deserializeJson(doc, message);

  if (error) {
    Serial.print(F("deserializeJson() failed: "));
    Serial.println(error.c_str());
    return true;
  }

  if (doc.containsKey("state")) {
    if (strcmp(doc["state"], MQTT_PAYLOAD_ON) == 0) {
      leds_state = true;
    }
    else if (strcmp(doc["state"], MQTT_PAYLOAD_OFF) == 0) {
      leds_state = false;
    }
  }

  if (doc.containsKey("brightness")) {
    const int leds_brightness_old = leds_getBrightness();
    leds_brightness = (int)doc["brightness"];

    // turn on if change
    if (leds_brightness_old != leds_brightness) {
      leds_state = true;
      leds_manual_change = true;
    }
  }

  if (doc.containsKey("color")) {
    const int leds_red_old = leds_getRed();
    leds_red = (int)doc["color"]["r"];

    // turn on if change
    if (leds_red_old != leds_red) {
      leds_state = true;
      leds_manual_change = true;
    }
  }

  if (doc.containsKey("color")) {
    const int leds_green_old = leds_getGreen();
    leds_green = (int)doc["color"]["g"];

    // turn on if change
    if (leds_green_old != leds_green) {
      leds_state = true;
      leds_manual_change = true;
    }
  }

  if (doc.containsKey("color")) {
    const int leds_blue_old = leds_getBlue();
    leds_blue = (int)doc["color"]["b"];

    // turn on if change
    if (leds_blue_old != leds_blue) {
      leds_state = true;
      leds_manual_change = true;
    }
  }

  if (doc.containsKey("white_value")) {
    const int leds_white_old = leds_getWhite();
    leds_white = (int)doc["white_value"];

    // turn on if change
    if (leds_white_old != leds_white) {
      leds_state = true;
      leds_manual_change = true;
    }
  }
  
  if (doc.containsKey("effect")) {
    String leds_effect_old = leds_getEffect();
    leds_effect = String((const char*)doc["effect"]);

    // turn on if change
    if (leds_effect_old != leds_effect) {
      leds_state = true;
    }
  }

  // If the user changed color/brightness/white manually, set effect to "Manual"
  if (leds_manual_change == true) {
    leds_effect = "Manual";
  }

  leds_update(leds_state, leds_brightness, leds_red, leds_green, leds_blue, leds_white, leds_effect);

  return false;
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived on topic ");
  Serial.print(topic);
  Serial.print(": ");

  char message[length + 1];
  for (unsigned int i = 0; i < length; i++) {
    message[i] = (char)payload[i];
  }
  message[length] = '\0';
  Serial.println(message);

  if (processJson(message)) {
    Serial.println("Error while parsing message");
    return;
  }

  mqtt_sendState();

  mqtt_client.loop();
}

void mqtt_check_reconnect() {
  if (!mqtt_client.connected()) {
    Serial.println("Reconnecting MQTT...");
    Serial.print("State: ");
    Serial.println(mqtt_client.state());

    if (!mqtt_client.connect(MQTT_CLIENTNAME)) {
      Serial.print("failed, rc=");
      Serial.println(mqtt_client.state());
    } else {
      mqtt_subscribe();
    }
  }
}

void mqtt_setup() {
  mqtt_client.setServer(MQTT_BROKER, 1883);
  mqtt_client.setCallback(callback);

  if (!mqtt_client.connect(MQTT_CLIENTNAME)) {
    Serial.print("failed, rc=");
    Serial.println(mqtt_client.state());
  } else {
    mqtt_subscribe();
    Serial.println("MQTT client started");
  }
}
