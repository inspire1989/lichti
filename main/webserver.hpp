#ifndef WEBSERVER_HPP
#define WEBSERVER_HPP

#include <ESP8266WebServer.h>

extern ESP8266WebServer webserver;

void webserver_setup();

#endif // WEBSERVER_HPP
