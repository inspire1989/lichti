#include "wifi.hpp"

#include <ESP8266WiFi.h>

const char* wifi_hostname = "lichti";

const char* wifi_ssid = "WIFI_SSID";
const char* wifi_password = "WIFI_PWD";

int wifi_retry = 0;

void wifi_check_reconnect() {
  
  if (wifi_retry < 5) {
	  if(WiFi.status() != WL_CONNECTED) {
		  wifi_retry++;
		  
		  Serial.println("WiFi not connected. Try to reconnect");
		  
		  WiFi.disconnect();
		  WiFi.mode(WIFI_OFF);
		  WiFi.mode(WIFI_STA);
		  WiFi.begin(wifi_ssid, wifi_password);
	  }
  } else {
      Serial.println("\nReboot");
      ESP.restart();
  }
}

void wifi_setup() {
  Serial.print("Connecting to WIFI: ");
  Serial.println(wifi_ssid);

  WiFi.mode(WIFI_STA);
  WiFi.hostname(wifi_hostname);

  WiFi.begin(wifi_ssid, wifi_password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(200);
    Serial.print(".");
    digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

  Serial.println("");
  Serial.print("Local IP: ");
  Serial.println(WiFi.localIP());
  Serial.print("Host name: ");
  Serial.println(wifi_hostname);
}
