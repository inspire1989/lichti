# lichti

![Lichti photo](/pictures/IMG-20200629-WA0006.jpg)

Arduino-based project that allows to control RGBW LED strips with the mqtt protocol. Works flawlessly in combination with the my raspberry-home-automation project.

The project supports arbitrary RGB colors as well as custom modes (rainbow, random, disco). It comes with gamma correction for proper color mixing and also it has software power limitation that ensures the LDE strip doesn't get too hot.

This project comes with:
- Arduino source code
- Drawio schematic for the circuitry
- Pictures for documentation
- A script to generate gamma correction values for the LEDs


## Set the WIFI SSID/password

Edit the file wifi.cpp and enter your wifi credentials at the top.


## Compilation

Use the Arduino IDE and add this repo URL:

http://arduino.esp8266.com/stable/package_esp8266com_index.json

Also select Board->ESP8266 and under "ESP8266 Boards" select "NodeMCU 1.0 (ESP-12E Module)"

Install the packages:

- PubSubClient
- ArduinoJson
- ArduinoOTA


## OTA flashing

Go to Tools -> Port and select "esp8266-Lichti at 192.168.178.X".

If the port for OTA is not showing up, start the "Bonjour browser" software and wait some time...


## HTTP access

Use the host name "lichti" to access with HTTP.

